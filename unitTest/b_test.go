package unittest

import (
	"net/http"
	"testing"

	"gotest.tools/assert"
)

func TestPostNewAlbum(t *testing.T) {
	for _, album := range Albums {
		responseWriter := makeRequest(
			http.MethodPost,
			"/v1/jwt/albums",
			album,
			true,
		)
		assert.Equal(t, http.StatusCreated, responseWriter.Code)
	}
}

func TestGetAlbums(t *testing.T) {
	responseWriter := makeRequest(
		http.MethodGet,
		"/v1/jwt/albums",
		nil,
		true,
	)
	assert.Equal(t, http.StatusOK, responseWriter.Code)
}

func TestGetAlbumByID(t *testing.T) {
	responseWriter := makeRequest(
		http.MethodGet,
		"/v1/jwt/albums/"+"1",
		nil,
		true,
	)
	assert.Equal(t, http.StatusOK, responseWriter.Code)
}

func TestPutAlbum(t *testing.T) {
	responseWriter := makeRequest(
		http.MethodPut,
		"/v1/jwt/albums/"+"1",
		PutAlbum,
		true,
	)
	assert.Equal(t, http.StatusOK, responseWriter.Code)
}

func TestDeleteAlbumByID(t *testing.T) {
	responseWriter := makeRequest(
		http.MethodDelete,
		"/v1/jwt/albums/"+"1",
		nil,
		true,
	)
	assert.Equal(t, http.StatusOK, responseWriter.Code)
}
